﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace _06
{
    class Program
    {
        static void Main(string[] args)
        {
            //Generate();
            using Context context = new();

            var courses = context.Courses.Include(c => c.StudentCourses).ThenInclude(sc => sc.Student).ToList();
            foreach (var c in courses)
            {
                Console.WriteLine($"\n\tCourse: {c.Name}");
                var students = c.StudentCourses.Select(sc => sc.Student).ToList();
                foreach(var s in students) Console.Write($"{s.Name}, ");
                Console.WriteLine("\b\b ");
            }

            Console.WriteLine("---------------------");
            Console.ReadKey();
        }
        static void Generate()
        {
            using Context context = new();
            Student s1 = new() {Name = "VovA"}, s2 = new() {Name = "Lena"}, s3 = new() {Name = "Masha"};
            context.Students.AddRange(s1, s2, s3);
            Course c1 = new() {Name = "C#"}, c2 = new() {Name = "C++"};
            context.Courses.AddRange(c1, c2);
            context.SaveChanges();
            s1.StudentCourses.Add(new StudentCourse {CourseId = c2.Id, StudentId = s1.Id});
            s2.StudentCourses.Add(new StudentCourse {CourseId = c1.Id, StudentId = s2.Id});
            s2.StudentCourses.Add(new StudentCourse {CourseId = c2.Id, StudentId = s2.Id});
            s3.StudentCourses.Add(new StudentCourse {CourseId = c1.Id, StudentId = s3.Id});
            context.SaveChanges();
        }
    }
}

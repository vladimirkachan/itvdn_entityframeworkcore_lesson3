﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01
{
    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }

        //public int TeamInfoKey { get; set; }
        [ForeignKey("TeamInfoKey")]
        public Team Team { get; set; }
    }
}

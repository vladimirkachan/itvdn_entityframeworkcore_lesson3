﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _04
{
    class Program
    {
        static void Main(string[] args)
        {
            using Context db = new();
            Generate(db);
            var users = db.Users;
            var profiles = db.UserProfiles;
            var q = from u in users 
                    join p in profiles
                    on u.Profile equals p
                    select new {u.Login, u.Password, p.Name, p.Age};
            foreach(var i in q)
                Console.WriteLine($"Login: {i.Login} | Password: {i.Password} | Name: {i.Name} | Age: {i.Age}");
            Console.ReadKey();
        }
        static void Generate(Context db)
        {
            var users = new List<User>
            {
                new() {Login = "masha", Password = "333", Profile = new UserProfile {Name = "Maria", Age = 30}},
                new() {Login = "vasya", Password = "444", Profile = new UserProfile {Name = "Vasiliy", Age = 25}}
            };
            db.Users.AddRange(users);
            db.SaveChanges();
        }
    }
}

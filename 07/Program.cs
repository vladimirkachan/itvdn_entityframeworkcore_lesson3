﻿using System;
using System.Linq;

namespace _07
{
    class Program
    {
        static void Main(string[] args)
        {
            //Generate();
            using Context db = new();
            var users = db.Users.ToList();
            Console.WriteLine("All users");
            foreach(var u in users) Console.Write($"{u.Name}, ");
            Console.WriteLine("\b\b ");
            Console.WriteLine("All employees");
            foreach(var u in db.Employees.ToList()) Console.Write($"{u.Name}, ");
            Console.WriteLine("\b\b ");
            Console.WriteLine("All managers");
            foreach(var u in db.Managers.ToList()) Console.Write($"{u.Name}, ");
            Console.WriteLine("\b\b ");
            Console.ReadKey();
        }
        static void Generate()
        {
            User a = new() {Name = "VovA"}, b = new() {Name = "Natasha"};
            using Context context = new();
            context.Users.AddRange(a, b);
            context.Employees.AddRange(new Employee {Name = "Olya",Salary=10000}, new Employee {Name = "Igor",Salary=12000});
            context.Managers.Add(new Manager {Name = "Vasya", Departament = "IT"});
            context.SaveChanges();
        }
    }
}

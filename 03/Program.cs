﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace _03
{
    class Program
    {
        static void Main(string[] args)
        {
            using Context db = new();
            Team team = db.Teams.Include(t => t.Players).Include(x => x.Stadium).FirstOrDefault();

            Console.WriteLine($"Team: {team.Name}");
            foreach (var p in team.Players)
                Console.WriteLine($"Player: {p.Name}    Country: {p.Country?.Name}");
            Console.WriteLine("-----------");

            team = db.Teams.Include(t => t.Players).ThenInclude(p => p.Country).FirstOrDefault();
            foreach (var p in team.Players) 
                Console.WriteLine($"Player: {p.Name,-10} Country: {p.Country?.Name}");
            Console.WriteLine("-----------");

            team = db.Teams.Include(t => t.Players)
                     .ThenInclude(p => p.Country)
                     .ThenInclude(c => c.Capital)
                     .Include(t => t.Stadium)
                     .FirstOrDefault();
            Console.WriteLine($"Team: {team.Name}, Stadium: {team.Stadium.Name}");
            foreach (var p in team.Players)
                Console.WriteLine($"Player: {p.Name,-10} Country: {p.Country?.Name,-14} Sity: {p.Country?.Capital.Name}");
            Console.WriteLine("-----------");

            team = db.Teams.FirstOrDefault();
            db.Players.Where(p => p.TeamId == team.Id).Load();
            Console.WriteLine($"Team: {team.Name}");
            foreach(var p in team.Players) Console.WriteLine($"Player: {p.Name}");
            Console.WriteLine("-----------");

            team = db.Teams.OrderBy(t => t.Id).LastOrDefault();
            db.Entry(team).Collection(t => t.Players).Load();
            Console.WriteLine($"Team: {team.Name}");
            foreach(var p in team.Players) Console.WriteLine($"Player: {p.Name}");
            Console.WriteLine("-----------");

            var player = db.Players.FirstOrDefault();
            db.Entry(player).Reference(x => x.Team).Load();
            Console.WriteLine($"Player: {player.Name} - Team: {player.Team.Name}");
            Console.WriteLine("----------------------");
            Console.ReadKey();
        }
        static void GenerateData()
        {
            using Context context = new();
            var teams = new List<Team>
            {
                new() {Name = "Богатыри", Stadium = new Stadium {Name = "Лужники-Арена"}},
                new() {Name = "Княгиня Ольга", Stadium = new Stadium {Name = "Печерский"}}
            };
            context.Teams.AddRange(teams);
            context.SaveChanges();

            var countries = new List<Country>
            {
                new() {Name = "Великая Русь", Capital = new City {Name = "Москва"}},
                new() {Name = "Киевская Русь", Capital = new City {Name = "Киев"}},
                new() {Name = "Белая Русь", Capital = new City {Name = "Минск"}}
            };
            context.Countries.AddRange(countries);
            context.SaveChanges();

            var players = new List<Player>
            {
                new() {Name = "Михаил", TeamId = 1, CountryId = 1},
                new() {Name = "Наташа", TeamId = 2, CountryId = 2},
                new() {Name = "Александр", TeamId = 1, CountryId = 3},
                new() {Name = "Оля", TeamId = 2, CountryId = 2},
                new() {Name = "Игорь", TeamId = 1, CountryId = 1},
                new() {Name = "Мария", TeamId = 2, CountryId = 3},
                new() {Name = "Николай", TeamId = 1, CountryId = 1},
                new() {Name = "Дарья", TeamId = 2, CountryId = 2},
                new() {Name = "Владимир", TeamId = 1, CountryId = 1}
            };
            context.Players.AddRange(players);
            context.SaveChanges();
        }
    }
}

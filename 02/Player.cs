﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02
{
    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int? TeamId { get; set; } // внешний ключ
        public Team Team { get; set; }  // навигационное свойство
    }
}

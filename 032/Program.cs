﻿using System;
using System.Collections.Generic;

namespace _032
{
    class Program
    {
        static void Main(string[] args)
        {
            GenerateData();
            Console.WriteLine("VovA");
            Console.ReadKey();
        }
        static void GenerateData()
        {
            using Lesson3_dbContext context = new();
            var teams = new List<Team>
            {
                new() {Name = "Богатыри", Stadium = new Stadium {Name = "Лужники-Арена"}},
                new() {Name = "Княгиня Ольга", Stadium = new Stadium {Name = "Печерский"}}
            };
            context.Teams.AddRange(teams);
            context.SaveChanges();

            var countries = new List<Country>
            {
                new() {Name = "Великая Русь", Capital = new City {Name = "Москва"}},
                new() {Name = "Киевская Русь", Capital = new City {Name = "Киев"}},
                new() {Name = "Белая Русь", Capital = new City {Name = "Минск"}}
            };
            context.Countries.AddRange(countries);
            context.SaveChanges();

            var players = new List<Player>
            {
                new() {Name = "Михаил", TeamId = 1, CountryId = 1},
                new() {Name = "Наташа", TeamId = 2, CountryId = 2},
                new() {Name = "Александр", TeamId = 1, CountryId = 3},
                new() {Name = "Оля", TeamId = 2, CountryId = 2},
                new() {Name = "Игорь", TeamId = 1, CountryId = 1},
                new() {Name = "Мария", TeamId = 2, CountryId = 3},
                new() {Name = "Николай", TeamId = 1, CountryId = 1},
                new() {Name = "Дарья", TeamId = 2, CountryId = 2},
                new() {Name = "Владимир", TeamId = 1, CountryId = 1}
            };
            context.Players.AddRange(players);
            context.SaveChanges();
        }

    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace _032
{
    public partial class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TeamId { get; set; }
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual Team Team { get; set; }
    }
}

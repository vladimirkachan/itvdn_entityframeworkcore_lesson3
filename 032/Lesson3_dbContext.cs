﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace _032
{
    public partial class Lesson3_dbContext : DbContext
    {
        public Lesson3_dbContext()
        {
        }

        public Lesson3_dbContext(DbContextOptions<Lesson3_dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<Stadium> Stadia { get; set; }
        public virtual DbSet<Team> Teams { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=.\\; Database=Lesson3_db; Trusted_Connection=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            modelBuilder.Entity<Country>(entity =>
            {
                entity.HasIndex(e => e.CapitalId, "IX_Countries_CapitalId");

                entity.HasOne(d => d.Capital)
                    .WithMany(p => p.Countries)
                    .HasForeignKey(d => d.CapitalId);
            });

            modelBuilder.Entity<Player>(entity =>
            {
                entity.HasIndex(e => e.CountryId, "IX_Players_CountryId");

                entity.HasIndex(e => e.TeamId, "IX_Players_TeamId");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Players)
                    .HasForeignKey(d => d.CountryId);

                entity.HasOne(d => d.Team)
                    .WithMany(p => p.Players)
                    .HasForeignKey(d => d.TeamId);
            });

            modelBuilder.Entity<Stadium>(entity =>
            {
                entity.ToTable("Stadium");
            });

            modelBuilder.Entity<Team>(entity =>
            {
                entity.HasIndex(e => e.StadiumId, "IX_Teams_StadiumId");

                entity.HasOne(d => d.Stadium)
                    .WithMany(p => p.Teams)
                    .HasForeignKey(d => d.StadiumId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace _032
{
    public partial class Team
    {
        public Team()
        {
            Players = new HashSet<Player>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int StadiumId { get; set; }

        public virtual Stadium Stadium { get; set; }
        public virtual ICollection<Player> Players { get; set; }
    }
}

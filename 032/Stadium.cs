﻿using System;
using System.Collections.Generic;

#nullable disable

namespace _032
{
    public partial class Stadium
    {
        public Stadium()
        {
            Teams = new HashSet<Team>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Team> Teams { get; set; }
    }
}
